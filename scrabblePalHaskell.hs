import System.Environment
import Data.Map as M
import Data.Maybe
import System.IO
import Data.List
 
filename :: Int -> String
filename n = "len" ++ (show n) ++ "words.txt"
  
getAnagrams :: [String] -> String -> [String]
getAnagrams word w = [x | x <- word, sort x == sort w]
  
letterScore :: Char -> Int
letterScore ch = fromJust $ M.lookup ch $ M.fromList [('a',1), ('b',3), ('c', 3), ('e', 1), ('d', 2), ('g', 2), ('f', 4), ('i', 1), ('h', 4), ('k', 5), ('j', 8), ('m', 3), ('l', 1), ('o', 1)    , ('n', 1), ('q', 10), ('p', 3), ('s', 1), ('r', 1), ('u', 1), ('t', 1), ('w', 4), ('v' , 4), (    'y', 4), ('x', 8), ('z', 10)]
  
wordScore :: String -> Int
wordScore wor = sum[letterScore ch | ch <- wor]
 
listWordScore ::[String] -> [Int]
listWordScore list = [wordScore wor | wor <- list]
 
constraints :: Int-> Char -> [String] -> [String]
constraints  num letter list = [x | x <- list , x !! (num - 1) == letter]
  
main = do
  args <- getArgs
  let string = args !! 0
  let character = args !! 1
  let ch = character !! 0
  let position = args !! 2
  let len = length string
  handle <- openFile (filename len) ReadMode
  contents <- hGetContents handle
  let finalList = getAnagrams (words contents) string
  let allwords = finalList
  let allwordsScore = listWordScore finalList
  let result2 = zip allwords allwordsScore
  let constraintList = constraints (read position:: Int) ch  finalList
  let constraintListScore = listWordScore $ constraints (read position:: Int) ch   finalList
  print result2
  let result = zip constraintList constraintListScore
  print result
hClose handle